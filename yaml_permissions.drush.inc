<?php

/**
 * @file
 * Drush execution of yaml-permission reverts.
 */

/**
 * Implements hook_drush_command().
 */
function yaml_permissions_drush_command() {

  $items['yaml-permissions-revert'] = array(
    'description' => 'Revert all permissions defined in modules self-contained permissions.yaml',
    'drupal dependencies' => array('yaml_permissions'),
    'arguments' => array(
      'extensions' => 'An optional list of modules or themes.',
    ),
    'aliases' => array('ypr'),
  );

  return $items;
}

/**
 * Pre-execution validation of yaml-permissions-revert command.
 */
function drush_yaml_permissions_revert_validate() {
  $args = pm_parse_arguments(func_get_args());
  $cleansed = array();
  if (!empty($args)) {
    foreach ($args as $arg) {
      if (!drush_module_exists($arg)) {
        drush_log(dt('The module !name is not installed.', array('!name' => $arg)), 'warning');
      }
      else {
        if ($module_path = drupal_get_path('module', $arg)) {
          $default_yaml_path = $module_path . DIRECTORY_SEPARATOR . 'permissions.yaml';
          $module_yaml_path = $module_path . DIRECTORY_SEPARATOR . $arg . '.permissions.yaml';
          if (file_exists($default_yaml_path) || file_exists($module_yaml_path)) {
            $cleansed[$arg] = $arg;
          }
          else {
            drush_log(dt('The module !name does not contain any packaged permissions.', array('!name' => $arg)), 'warning');
          }
        }
      }
    }
  }
  else {
    // Called with no arguments - assume all modules.
    $modules = drush_module_list();
    $cleansed = array_filter($modules, '_yaml_permissions_filter_discover');
  }

  drush_set_context('YAML_PERMISSIONS_REVERT', $cleansed);
}

/**
 * Drush callback for revert.
 */
function drush_yaml_permissions_revert() {
  $modules = drush_get_context('YAML_PERMISSIONS_REVERT');
  if (!empty($modules)) {
    drush_print(dt('The following modules will have permissions reverted: !modules', array('!modules' => implode(', ', $modules))));
    if (!drush_confirm(dt('Do you want to continue?'))) {
      return drush_user_abort();
    }
    foreach ($modules as $module) {
      yaml_permissions_revert_module($module);
      drush_log(dt('Permissions were reverted for !module.', array('!module' => $module)), 'ok');
    }
  }
}

/**
 * Command argument complete callback.
 */
function yaml_permissions_yaml_permissions_revert_complete() {
  if (drush_bootstrap_max(DRUSH_BOOTSTRAP_DRUPAL_FULL)) {
    $extension_info = drush_get_extensions(FALSE);
    $module_list = array_keys($extension_info);
    $clean_module_list = array_filter($module_list, '_yaml_permissions_filter_discover');
    return array('values' => $clean_module_list);
  }
}
