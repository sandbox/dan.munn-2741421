<?php

/**
 * @file
 * Functionality for yaml_permissions module.
 */

/**
 * Implements hook_menu().
 */
function yaml_permissions_menu() {

  $items['admin/config/people/yaml-permissions'] = array(
    'title' => 'Revert permissions',
    'description' => 'Revert stored permissions back to packaged defaults with YAML permissions.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('yaml_permissions_admin_revert'),
    'access arguments' => array('administer permissions'),
    'file' => 'yaml_permissions.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/people/yaml-permissions/list'] = array(
    'title' => 'Individual modules',
    'description' => 'Revert individual module permissions.',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/config/people/yaml-permissions/revert'] = array(
    'title' => 'Revert all',
    'description' => 'Revert all stored permissions back to packaged defaults.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('yaml_permissions_admin_revert_all'),
    'access arguments' => array('administer permissions', 'yaml_permissions revert all'),
    'file' => 'yaml_permissions.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  return $items;
}

/**
 * Implements hook_admin_paths().
 */
function yaml_permissions_admin_paths() {
  return array(
    'admin/config/people/yaml-permissions',
    'admin/config/people/yaml-permissions/list',
    'admin/config/people/yaml-permissions/revert',
  );
}

/**
 * Implements hook_permission().
 */
function yaml_permissions_permission() {
  return array(
    'yaml_permissions revert all' => array(
      'title' => t('Revert all permissions'),
      'description' => t('Revert all packaged permissions back to their default state as defined in YAML files packaged with modules.'),
    ),
  );
}

/**
 * Utility function to retrieve list of modules that contain permissions.
 *
 * @param bool $reset
 *   DEFAULT FALSE; TRUE indicates internal caches should be cleared.
 *
 * @return array
 *   Returns an array of modules that implement permissions.
 */
function yaml_permissions_discover($reset = FALSE) {
  static $implements;

  if ($reset) {
    // Reset EVERYTHING.
    unset($implements);
    drupal_static_reset(__FUNCTION__);
    cache_clear_all(__FUNCTION__, 'cache');
  }

  $loaded_from_cache = FALSE;
  if (!isset($implements)) {
    $implements =& drupal_static(__FUNCTION__, array());
  }

  if (empty($implements)) {
    if ($cache = cache_get(__FUNCTION__)) {
      $implements = $cache->data;
      $loaded_from_cache = TRUE;
    }

    if (!$loaded_from_cache && empty($implements)) {
      // Build up the cache.
      $module_list = module_list();
      $clean_module_list = array_filter($module_list, '_yaml_permissions_filter_discover');
      foreach ($clean_module_list as $module_name => &$item) {
        $module_path = drupal_get_path('module', $module_name);
        $default_yaml_path = $module_path . DIRECTORY_SEPARATOR . 'permissions.yaml';
        $module_yaml_path = $module_path . DIRECTORY_SEPARATOR . $module_name . '.permissions.yaml';
        $item = $module_yaml_path;
        if (file_exists($default_yaml_path)) {
          $item = $default_yaml_path;
        }
      }
      cache_set(__FUNCTION__, $clean_module_list);
      $implements =& $clean_module_list;
    }
  }
  return $implements;
}

/**
 * Filter callback: remove irrelevant modules from list.
 *
 * This function will take steps to where appropriate validate the array
 * provided to it and remove items that don't contains necessary files.
 *
 * @param string $module
 *   The name of the module being filtered.
 *
 * @return bool
 *   Indicator if item should be removed or preserved.
 */
function _yaml_permissions_filter_discover($module) {
  if (module_exists($module)) {
    if ($module_path = drupal_get_path('module', $module)) {
      $default_yaml_path = $module_path . DIRECTORY_SEPARATOR . 'permissions.yaml';
      $module_yaml_path = $module_path . DIRECTORY_SEPARATOR . $module . '.permissions.yaml';
      return (file_exists($default_yaml_path) || file_exists($module_yaml_path));
    }
  }
  return FALSE;
}


/**
 * Load and sync all permissions provided by module.
 *
 * Order of function:
 * 1. Does module exist
 * 2. Does module contain permissions.yaml / <module>.permissions.yaml
 * 3. Load and parse YAML file
 * 4. Parse and distribute.
 *
 * @param string $module
 *   Module being updated.
 * @param bool $commit_at_end
 *   Define if it commits cached global rights.
 */
function yaml_permissions_revert_module($module, $commit_at_end = TRUE) {
  $_modules = yaml_permissions_discover();
  if (isset($_modules[$module]) && $file_name = $_modules[$module]) {
    if (file_exists($file_name)) {
      // Iterate through sections to ensure that we do the correct sync
      // operations.
      $yaml_input = _yaml_permissions_parse($file_name);
      $function_base = '_yaml_permissions_sync_';

      // To support cross communication (if required).
      $pipe = array(
        'roles' => user_roles(),
      );
      foreach ($yaml_input as $section => $values) {
        if (preg_match('/^[a-zA-Z0-9]+$/', $section)) {
          $function_name = $function_base . $section;
          if (function_exists($function_name)) {
            $function_name($values, $pipe, $yaml_input);
          }
          // TODO: Add logging in case of failure.
        }
      }
      unset($pipe);
    }
    if ($commit_at_end) {
      _yaml_permissions_commit_grant_all();
    }
  }
}

/**
 * Utility function to store permissions when being set against all roles.
 *
 * @param string $module
 *   Module being used for storage.
 * @param string $permission
 *   Permission being stored.
 */
function _yaml_permissions_queue_grant_all($module, $permission) {
  $fast_access =& drupal_static('yaml_permissions_queue_grant', array());
  $fast_access[$module][$permission] = $permission;
}

/**
 * Commit grant alls saved in fast cache to all active roles.
 */
function _yaml_permissions_commit_grant_all() {
  $fast_access =& drupal_static('yaml_permissions_queue_grant', array());
  $roles = user_roles();
  foreach ($roles as $rid => $role_name) {
    foreach ($fast_access as $module => $permissions) {
      foreach ($permissions as $permission) {
        try {
          db_merge('role_permission')
            ->key(array(
              'rid' => $rid,
              'permission' => $permission,
            ))
            ->fields(array(
              'module' => $module,
            ))
            ->execute();
        }
        catch (Exception $e) {
          // TODO: We should really be commiting this for later attempt.
        }
      }
    }
  }
}

/**
 * Internal utility function to sync up permissions from YAML.
 *
 * @param string $permissions
 *   Defined permissions.
 * @param array $pipe
 *   Defined pipe.
 * @param array $source
 *   Source (where appropriate).
 */
function _yaml_permissions_sync_permissions($permissions, &$pipe = array(), &$source = array()) {
  foreach ($permissions as $module => $permissions) {
    foreach ($permissions as $permission => $configuration) {
      if ($module == 'taxonomy') {
        _yaml_permissions_change_term_permission($permission);
      }

      if (is_string($configuration) && trim($configuration) === '*') {
        _yaml_permissions_queue_grant_all($module, $permission, $pipe);
      }
      elseif (is_array($configuration)) {
        _yaml_permissions_sync_permissions_roles($module, $permission, $configuration, $pipe);
      }
    }
  }
}

/**
 * Identification and routing of permission storage based on configuration.
 *
 * @param string $module
 *   Module where permission is defined.
 * @param string $permission
 *   Permission being defined.
 * @param array $configuration
 *   Configuration nested within permission / role.
 * @param array $pipe
 *   Shared (defined) pipe.
 */
function _yaml_permissions_sync_permissions_roles($module, $permission, $configuration, &$pipe) {
  $roles = array();

  // Better safe than sorry.
  $pipe['roles'] = $pipe['roles'] ? $pipe['roles'] : user_roles();
  if (!isset($pipe['roles_flipped'])) {
    $pipe['roles_flipped'] = array_flip($pipe['roles']);
  }
  if (isset($configuration['roles'])) {
    if (is_string($configuration['roles']) && trim($configuration['roles']) === '*') {
      yaml_permissions_queue_grant_all($module, $permission);
      return;
    }
    foreach ($configuration['roles'] as $role) {
      if (!isset($pipe['roles_flipped'][$role])) {
        continue;
      }
      $roles[] = $pipe['roles_flipped'][$role];
    }

  }

  /*
   * Check for higher-level role definition e.g.:
   *  permissions:
   *    permission name:
   *      role 1
   */
  unset($configuration['roles']);
  foreach ($configuration as $role) {
    if (!is_array($role)) {
      if (!isset($pipe['roles_flipped'][$role])) {
        continue;
      }
      $roles[] = $pipe['roles_flipped'][$role];
    }
  }
  if (!empty($roles)) {
    foreach ($roles as $role) {
      try {
        user_role_grant_permissions($role, array($permission));
      }
      catch (Exception $e) {
        // TODO: Catch this permission for later inclusion.
      }
    }
  }
}

/**
 * Convert taxonomy permission between using vocabulary id and machine name.
 *
 * @param string $permission
 *   Full permission name.
 * @param string $type
 *   The type of term that is in the permission parameter.
 *   Either 'machine_name' or 'vid'.
 */
function _yaml_permissions_change_term_permission(&$permission, $type = 'machine_name') {
  if (!module_exists('taxonomy')) {
    return;
  }
  // Export vocabulary permissions using the machine name, instead of vocabulary
  // id.
  if (strpos($permission, 'edit terms in ') !== FALSE || strpos($permission, 'delete terms in ') !== FALSE) {
    preg_match("/(?<=\040)([^\s]+?)$/", trim($permission), $voc_id);
    $vid = $voc_id[0];

    // If type is machine_name try to get the vid.
    if ($type == 'machine_name') {
      if ($voc = taxonomy_vocabulary_machine_name_load($vid)) {
        $permission = str_replace($vid, $voc->vid, $permission);
      }
    }
    // Otherwise try to get the machine_name.
    elseif (is_numeric($vid) && $type == 'vid') {
      if (function_exists('taxonomy_vocabulary_load')) {
        if ($voc = taxonomy_vocabulary_load($vid)) {
          $permission = str_replace($vid, $voc->machine_name, $permission);
        }
      }
    }
  }
}

/**
 * Parse a YAML stream.
 *
 * @param string $input
 *   The YAML stream.
 *
 * @return array
 *   Array of parsed YAML elements.
 */
function _yaml_permissions_parse($input) {
  if (function_exists('yaml_parse')) {
    if (file_exists($input) && function_exists('yaml_parse_file')) {
      return yaml_parse_file($input);
    }
    elseif (file_exists($input)) {
      $input = file_get_contents($input);
    }
    return yaml_parse($input);
  }
  elseif (($library = libraries_load('spyc')) && !empty($library['loaded'])) {
    if (file_exists($input)) {
      return spyc_load_file($input);
    }
    else {
      return spyc_load($input);
    }
  }
  return FALSE;
}
