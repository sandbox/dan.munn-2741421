<?php

/**
 * @file
 * Administrative functions/interface for YAML permissions.
 */

/**
 * Configuration form for permissions.
 *
 * @ingroup form.
 */
function yaml_permissions_admin_revert_all($form, &$form_state) {

  return confirm_form($form,
    t('Are you sure you want to revert all permissions'),
    'admin/config/people/yaml-permissions',
    t('This action will overwrite any changes from packaged defaults, but will not disable any permissions not explicitly disabled in packaged yaml files.'),
    t('Revert'),
    t('Cancel')
  );
}

/**
 * Submission callback for yaml_permissions_admin_revert_all form.
 *
 * @see yaml_permissions_admin_revert_all()
 */
function yaml_permissions_admin_revert_all_submit() {
  $batch = array(
    'title' => t('Reverting permissions'),
    'operations' => array(
      array('yaml_permissions_batch_revert_all', array()),
    ),
    'file' => drupal_get_path('module', 'yaml_permissions') . '/yaml_permissions.batch.inc',
    'finished' => 'yaml_permissions_batch_revert_finished',
    'progress_message' => '',
  );
  batch_set($batch);
  batch_process();
}
