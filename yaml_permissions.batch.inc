<?php

/**
 * @file
 * Definition of functionality using batch API.
 */

/**
 * Batch callback for reverting all permissions.
 *
 * @param array $context
 *   The batch context array, passed by reference.
 */
function yaml_permissions_batch_revert_all(&$context) {

  // If we have no sandbox set create it.
  if (empty($context['sandbox'])) {
    // Get all of the modules that contain permissions.
    $module_list = yaml_permissions_discover(TRUE);
    $context['sandbox'] = array(
      'progress' => 0,
      'modules' => array_keys($module_list),
      'max' => count($module_list),
    );
  }

  if (!empty($context['sandbox']['modules'])) {

    // Remove the first item from the modules containing permissions.s.
    $module = array_shift($context['sandbox']['modules']);

    $count = count($context['sandbox']['modules']);

    $context['message'] = format_plural(
      $count,
      t('Processing @module. @count module remaining.', array('@module' => $module, '@count' => $count)),
      t('Processing @module. @count modules remaining.', array('@module' => $module, '@count' => $count))
    );

    // Revert all of the permissions for the module.
    yaml_permissions_revert_module($module);

    // Calculate the percentage of modules left to process.
    $context['finished'] = $context['sandbox']['max'] ?
      ($context['sandbox']['max'] - $count) / $context['sandbox']['max'] : 1;

    // Add the module processed to the results array so that we can count how
    // many have been processed after the batch has completed.
    $context['results'][] = $module;
  }
  else {
    $context['finished'] = 1;
  }
}

/**
 * Callback after the reset batch process has finished.
 *
 * @param bool $success
 *   A boolean indicating whether the batch has completed successfully.
 * @param array $result
 *   The value set in $context['results'] by
 *   yaml_permissions_batch_revert_all().
 */
function yaml_permissions_batch_revert_finished($success, $result) {
  if ($success) {
    $count = count($result);
    $message = format_plural(
      $count,
      t('Successfully reverted permissions from @count module', array('@count' => $count)),
      t('Successfully reverted permissions from @count modules', array('@count' => $count))
    );
  }
  else {
    $message = t('There was an error reverting the permissions.');
  }

  drupal_set_message($message);
  drupal_goto('admin/config/people/yaml-permissions');
}
